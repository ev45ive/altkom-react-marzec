import axios, { AxiosRequestConfig } from "axios"
import { Album, AlbumsSearchRespose } from "../model/Search"



export const fetchSearchResults = (query: string, options?: AxiosRequestConfig) => {

    return axios.get<AlbumsSearchRespose>(`https://api.spotify.com/v1/search`, {
        params: {
            type: 'album',
            q: query
        },
        ...options
    }).then(res => (res.data.albums.items))
}


export const fetchAlbumById = (id: string, options?: AxiosRequestConfig) => {

    return axios.get<Album>(`https://api.spotify.com/v1/albums/${id}`).then(res=>res.data)
}