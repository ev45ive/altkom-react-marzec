import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import PlaylistsView from './playlists/containers/PlaylistsView';
import AlbumSearch from './search/containers/AlbumSearch';

import {
  Switch,
  Route,
  Redirect,
  NavLink
} from "react-router-dom";
import { NavBarUser } from './core/contexts/NavBarUser';
import AlbumDetails from './search/containers/AlbumDetails';

export function NavBar() {
  const [isOpen, setIsOpen] = useState(false)

  return (<>
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
      <div className="container">
        <NavLink className="navbar-brand" to="/">MusicApp</NavLink>

        <button className="navbar-toggler" type="button" aria-controls="navbarNav" aria-expanded={isOpen} aria-label="Toggle navigation" onClick={() => setIsOpen(!isOpen)}>
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className={`collapse navbar-collapse ${isOpen && 'show'}`} id="navbarNav">
          <ul className="navbar-nav">

            <li className="nav-item">
              <NavLink className="nav-link" to="/search" activeClassName="placki active">Search</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
            </li>

          </ul>
          <NavBarUser />
        </div>
      </div>
    </nav>
  </>)
}

function App() {
  return (<>
    <NavBar />

    <div className="container">
      {/* .container>.row>.col */}

      <div className="row">
        <div className="col">

          <Switch>
            <Redirect path="/" exact={true} to="/playlists" />
            {/* === */}
            <Route path="/playlists/:playlist_id?" exact={true} component={PlaylistsView as any} />
            <Route path="/search" component={AlbumSearch} />
            <Route path="/albums/:album_id?" component={AlbumDetails} />
            {/* === */}
            <Route path="**" render={() => <h1>Page not found</h1>} />
          </Switch>


          {/* <Map>
            {locations.map(loc => <Marker loc={loc}/>)}
          </Map> */}

        </div>
      </div>

    </div></>
  );
}

export default App;
