import React, { useEffect, useRef, useState } from 'react'
import { useParams } from 'react-router'
import { RouteComponentProps } from 'react-router-dom'
import { Album, Track } from '../../core/model/Search'
import { fetchAlbumById } from '../../core/services/MusicSearch'
import AlbumCard from '../components/AlbumCard'

// http://localhost:3000/albums/5Tby0U5VndHW0SomYO7Id7

// render component route
// display Id from params
// use fetchAlbumById to fetch album
// dispaly album title
// display AlbumCard with album data
// ...
// Link from album card

interface Props { }

const Loading = () => <div className="d-flex justify-content-center text-primary">
    <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
    </div>
</div>


const AlbumDetails = (props: Props /* & RouteComponentProps<{ album_id: string }> */) => {
    // const = album_id props.match.params['album_id']

    const { album_id } = useParams<{ album_id: string }>()
    // const history = useHistory()

    const [message, setMessage] = useState('')
    const [album, setAlbum] = useState<Album | null>(null)
    const [currentTrack, setCurrentTrack] = useState<Track | null>(null)

    useEffect(() => {
        setMessage('')
        fetchAlbumById(album_id)
            .then(res => setAlbum(res))
            .catch(err => setMessage(err.message))
    }, [])

    const audioRef = useRef<HTMLAudioElement>(null)

    const playTrack = (nextTrack: Track) => {
        setCurrentTrack(nextTrack)
    }

    useEffect(() => {
        if (audioRef.current?.src !== currentTrack?.preview_url) {
            return
        }
        if (audioRef.current) {
            audioRef.current.volume = 0.1
            audioRef.current.play()
        }
    }, [currentTrack])

    if (message) {
        return <p className="alert alert-danger">{message}</p>
    }

    if (!album) {
        return <Loading />
    }

    return (
        <div>

            <div className="row">
                <div className="col">
                    {album && <AlbumCard album={album} />}

                </div>
                <div className="col">
                    <small>{album_id}</small>
                    <h3>{album?.name}</h3>

                    <dl>
                        <dt>Artist</dt>
                        <dd>{album.artists[0].name}</dd>
                        <dt>Release date</dt>
                        <dd>{album.release_date}</dd>
                    </dl>

                    <hr />

                    <audio src={currentTrack?.preview_url} controls={true} className="w-100 my-2 " ref={audioRef} />

                    {/* .list-group>.list-group-item*5{Track $} */}

                    <div className="list-group">
                        {album.tracks?.items.map(track => {
                            return <div className="list-group-item" key={track.id} >
                                {track.name}
                                <span className="float-right" onClick={() => playTrack(track)}>
                                    <i className={`bi bi-play${currentTrack?.id === track.id ? '-fill' : ''} `}></i>
                                </span>
                            </div>
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AlbumDetails
