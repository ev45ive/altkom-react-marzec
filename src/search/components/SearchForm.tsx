import React, { useEffect, useRef, useState } from 'react'

interface Props {
  query: string
  onSearch(query: string): void
}

const SearchForm = ({ onSearch, query: parentQuery }: Props) => {

  const [query, setQuery] = useState(parentQuery)
  const inputRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    setQuery(parentQuery)
  }, [parentQuery])

  useEffect(() => {
    inputRef.current?.focus()
  }, [])

  // useEffect(() => {
  //   effect
  //   return () => {
  //     cleanup
  //   }
  // }, [input])


  useEffect(() => {
    const handle = setTimeout(() => {
      query && onSearch(query)
      // console.log(query)
    }, 400)

    return () => clearTimeout(handle)
  }, [query]);

  return (
    <div>
      <div className="input-group mb-3">

        <input type="text" className="form-control" placeholder="Search"
          ref={inputRef}
          value={query}
          onKeyUp={e => e.code === 'Enter' && setQuery((e.target as HTMLInputElement).value)}
          onChange={e => setQuery(e.target.value)} />


        {/* <button className="btn btn-outline-secondary" type="button"
          onClick={() => onSearch(query)}>
          Search
        </button> */}
      </div>
    </div>
  )
}

export default SearchForm
