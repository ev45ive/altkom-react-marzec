
```js
stack = [];

getState = () => stack[0];
setState = (s) => stack[0] = s;

useState = () => [stack[0], setState]


function PseudoComponent(){

    // let albums = getState()
    let [albums,setAlbums] = getState()

    const getAlbums = () => { setAlbums([1,2,3]) }

    return {
        type:'div', onClick: getAlbums, children: albums 
    }
}

vdom = PseudoComponent()
console.log(vdom);
vdom.onClick()

vdom = PseudoComponent()
console.log(vdom);

```